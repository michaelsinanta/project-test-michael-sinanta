import { Banner } from "@/components/elements/Banner";
import { Navbar } from "@/components/elements/Navbar";
import { IdeasModule } from "@/components/modules/IdeasModule";

export default function Work() {
  return (
    <div>
      <Navbar />
      <Banner
        image={"/assets/banner/about.jpeg"}
        title={"About Us"}
        description={"Where all our information displayed"}
      />
    </div>
  );
}
