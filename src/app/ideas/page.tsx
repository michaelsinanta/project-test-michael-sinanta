import { Banner } from "@/components/elements/Banner";
import { Navbar } from "@/components/elements/Navbar";
import { IdeasModule } from "@/components/modules/IdeasModule";

export default function Ideas() {
  return (
    <div>
      <Navbar />
      <Banner
        image={"/assets/banner/ideas.jpeg"}
        title={"Ideas"}
        description={"Where all our great things begin"}
      />
      <IdeasModule />
    </div>
  );
}
