import { Banner } from "@/components/elements/Banner";
import { Navbar } from "@/components/elements/Navbar";
import { IdeasModule } from "@/components/modules/IdeasModule";

export default function Work() {
  return (
    <div>
      <Navbar />
      <Banner
        image={"/assets/banner/works.jpeg"}
        title={"Work"}
        description={"Where all our works begin"}
      />
    </div>
  );
}
