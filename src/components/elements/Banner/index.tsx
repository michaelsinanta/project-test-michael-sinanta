"use client";
import { ParallaxBanner, ParallaxProvider } from "react-scroll-parallax";
import { BannerProps } from "./interface";

export const Banner: React.FC<BannerProps> = ({
  image,
  title,
  description,
}) => {
  return (
    <ParallaxProvider>
      <ParallaxBanner
        className="overflow-hidden card-header lg:aspect-[3/1] md:aspect-[2/1] aspect-[5/4]"
        layers={[{ image: `${image}`, speed: -15 }]}
      >
        <div className="relative w-full flex flex-col items-center justify-center text-white z-10 h-5/6">
          <h1 className="text-4xl font-bold">{title}</h1>
          <p className="text-xl mt-2">{description}</p>
        </div>
      </ParallaxBanner>
    </ParallaxProvider>
  );
};
