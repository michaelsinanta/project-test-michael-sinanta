export interface ImageInfo {
  id: number;
  mime: string;
  file_name: string;
  url: string;
}

export interface CardProps {
  content: string;
  created_at: string;
  deleted_at: string | null;
  id: number;
  medium_image: ImageInfo[];
  published_at: string;
  slug: string;
  small_image: ImageInfo[];
  title: string;
  updated_at: string;
}
