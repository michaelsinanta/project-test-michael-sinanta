import Image from "next/image";
import { CardProps } from "./interface";
import { useState } from "react";

export const IdeasCard: React.FC<CardProps> = (props) => {
  const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    const day = date.getDate();
    const months = [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Agustus",
      "September",
      "Oktober",
      "November",
      "Desember",
    ];
    const month = months[date.getMonth()];
    const year = date.getFullYear();
    return `${day} ${month} ${year}`;
  };

  const [imgSrc, setImgSrc] = useState(
    props.small_image[0]?.url || props.medium_image[0]?.url,
  );

  const handleImgError = () => {
    setImgSrc("/assets/no-image.png");
  };

  return (
    <>
      <div className="rounded-md flex flex-col shadow-md text-start">
        <div className="relative w-full h-48 overflow-hidden">
          <Image
            src={imgSrc}
            alt={`Image of ${props.title}`}
            onError={handleImgError}
            layout="fill"
            loading="lazy"
          />
        </div>

        <div className="p-5">
          <div className="text-gray-400 text-md font-bold">
            {formatDate(props.published_at).toUpperCase()}
          </div>
          <div className="text-lg line-clamp-3 font-bold">{props.title}</div>
        </div>
      </div>
    </>
  );
};
