"use client";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useEffect, useRef, useState } from "react";

const NAV_ITEMS = [
  { href: "/work", label: "Work" },
  { href: "/about", label: "About" },
  { href: "/services", label: "Services" },
  { href: "/ideas", label: "Ideas" },
  { href: "/careers", label: "Careers" },
  { href: "/contact", label: "Contact" },
];

export const Navbar: React.FC = () => {
  const pathname = usePathname();
  const [prevScrollPos, setPrevScrollPos] = useState(0);
  const [hidden, setHidden] = useState(true);
  const [transparent, setTransparent] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollPos = window.scrollY;
      setHidden(prevScrollPos > currentScrollPos || currentScrollPos < 10);
      if (currentScrollPos === 0) {
        setTransparent(false);
      } else {
        setTransparent(true);
      }
      setPrevScrollPos(currentScrollPos);
    };

    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  }, [prevScrollPos]);

  return (
    <>
      <header
        className={`${hidden ? "top-0" : "top-[-100px]"} ${
          transparent ? "opacity-80" : "opacity-100"
        } fixed w-full bg-[#ff6600] text-white p-0 flex items-center justify-between overflow-hidden z-20 transition-all duration-100 lg:px-20 md:px-10 px-1`}
      >
        <Image
          src={"/assets/logo.webp"}
          alt={""}
          width={150}
          height={150}
          className="logos px-6 py-4"
        ></Image>
        <ul className="flex overflow-x-auto space-x-4">
          {NAV_ITEMS.map(({ href, label }) => {
            const isActive = pathname === href;
            return (
              <NavLink
                key={href}
                href={href}
                isActive={isActive}
                label={label}
              />
            );
          })}
        </ul>
      </header>
    </>
  );
};

type NavLinkProps = {
  href: string;
  isActive: boolean;
  label: string;
};

const NavLink = ({ href, isActive, label }: NavLinkProps) => {
  return (
    <li>
      <Link href={href} className={`${isActive ? "underline" : ""}`}>
        <p className="relative after:bg-white after:absolute after:h-[2px] after:w-0 after:bottom-0 after:left-0 hover:after:w-full after:transition-all after:duration-300 cursor-pointer">
          {label}
        </p>
      </Link>
    </li>
  );
};
