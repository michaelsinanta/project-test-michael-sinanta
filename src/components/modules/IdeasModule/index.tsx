"use client";

import { useEffect, useState } from "react";
import axios from "axios";
import { IdeasCard } from "@/components/elements/IdeasCard";
import { CardProps } from "@/components/elements/IdeasCard/interface";
import {
  MdKeyboardDoubleArrowLeft,
  MdKeyboardDoubleArrowRight,
  MdKeyboardArrowLeft,
  MdKeyboardArrowRight,
} from "react-icons/md";

export const IdeasModule: React.FC = () => {
  const [ideas, setIdeas] = useState<CardProps[]>([]);

  const setToLocalStorage = (key: string, value: any) => {
    if (typeof window !== "undefined") {
      localStorage.setItem(key, JSON.stringify(value));
    }
  };

  const getFromLocalStorage = (key: string, defaultValue: any) => {
    if (typeof window !== "undefined") {
      const storedValue = localStorage.getItem(key);
      return storedValue ? JSON.parse(storedValue) : defaultValue;
    }
    return defaultValue;
  };

  const [currentPage, setCurrentPage] = useState(() =>
    getFromLocalStorage("currentPage", 1)
  );
  const [itemsPerPage, setItemsPerPage] = useState(() =>
    getFromLocalStorage("itemsPerPage", 10)
  );
  const [sortOrder, setSortOrder] = useState(() =>
    getFromLocalStorage("sortOrder", "-published_at")
  );
  const [totalItems, setTotalItems] = useState(() =>
    getFromLocalStorage("totalItems", 0)
  );

  useEffect(() => {
    const fetchIdeas = async () => {
      const response = await axios.get(
        `https://suitmedia-backend.suitdev.com/api/ideas`,
        {
          params: {
            "page[number]": currentPage,
            "page[size]": itemsPerPage,
            append: ["small_image", "medium_image"],
            sort: sortOrder,
          },
        }
      );
      setIdeas(response.data.data);
      setTotalItems(response.data.meta.total);
    };

    fetchIdeas();

    setToLocalStorage("currentPage", currentPage);
    setToLocalStorage("itemsPerPage", itemsPerPage);
    setToLocalStorage("sortOrder", sortOrder);
  }, [currentPage, itemsPerPage, sortOrder]);

  const totalPages = Math.ceil(totalItems / itemsPerPage);

  const handleFirstPage = () => setCurrentPage(1);
  const handleLastPage = () => setCurrentPage(totalPages);
  const handleNextPage = () =>
    setCurrentPage((prev: number) => Math.min(prev + 1, totalPages));
  const handlePreviousPage = () =>
    setCurrentPage((prev: number) => Math.max(prev - 1, 1));
  const handlePageNumberClick = (page: number) => {
    setCurrentPage(page);
  };

  const getVisiblePages = () => {
    const visiblePages = [];
    const maxPagesToShow = 5;

    const startPage = Math.max(1, currentPage - Math.floor(maxPagesToShow / 2));
    const endPage = Math.min(totalPages, startPage + maxPagesToShow - 1);

    for (let i = startPage; i <= endPage; i++) {
      visiblePages.push(i);
    }

    return visiblePages;
  };

  const visiblePages = getVisiblePages();

  return (
    <>
      <main className="lg:mx-32 md:mx-12 mx-5 mt-5 mb-12">
        <div>
          <div className="flex lg:flex-row md:flex-row flex-col justify-between lg:items-center">
            <p className="font-medium">
              Showing{" "}
              {currentPage === 1 ? 1 : (currentPage - 1) * itemsPerPage + 1} -
              {Math.min(currentPage * itemsPerPage, totalItems)} of {totalItems}
            </p>
            <div className="flex lg:flex-row md:flex-row flex-col lg:space-x-5 md:space-x-5 space-x-0">
              <div className="flex flex-row space-x-2">
                <p className="font-medium">Show per page:</p>
                <select
                  value={sortOrder}
                  onChange={(e) => setSortOrder(e.target.value)}
                  className="border-2 border-gray-300 rounded-2xl w-28"
                >
                  <option value="-published_at">Newest</option>
                  <option value="published_at">Oldest</option>
                </select>
              </div>
              <div className="flex flex-row space-x-2">
                <p className="font-medium">Sort by:</p>
                <select
                  value={itemsPerPage}
                  onChange={(e) => setItemsPerPage(Number(e.target.value))}
                  className="border-2 border-gray-300 rounded-2xl w-28"
                >
                  <option value="10">10</option>
                  <option value="20">20</option>
                  <option value="50">50</option>
                </select>
              </div>
            </div>
          </div>

          <div className="mt-8 grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 lg:gap-8">
            {ideas.map((idea) => (
              <IdeasCard key={idea.id} {...idea} />
            ))}
          </div>

          <div className="flex items-center justify-center space-x-3 m-12">
            <div
              className={`${
                currentPage === 1
                  ? "text-gray-400 cursor-not-allowed"
                  : "text-black cursor-pointer"
              } flex space-x-3`}
            >
              <MdKeyboardDoubleArrowLeft onClick={handleFirstPage} size={30} />
              <MdKeyboardArrowLeft onClick={handlePreviousPage} size={30} />
            </div>
            {visiblePages.map((pageNumber) => (
              <span
                key={pageNumber}
                className={`cursor-pointer px-3 py-1 ${
                  currentPage === pageNumber
                    ? "bg-[#ff6600] text-white rounded-lg font-bold"
                    : "text-black"
                }`}
                onClick={() => handlePageNumberClick(pageNumber)}
              >
                {pageNumber}
              </span>
            ))}

            <div
              className={`${
                currentPage === totalPages
                  ? "text-gray-400 cursor-not-allowed"
                  : "text-black cursor-pointer"
              } flex space-x-3`}
            >
              <MdKeyboardArrowRight onClick={handleNextPage} size={30} />
              <MdKeyboardDoubleArrowRight onClick={handleLastPage} size={30} />
            </div>
          </div>
        </div>
      </main>
    </>
  );
};
